import { Component, OnInit, EventEmitter, Output } from "@angular/core";

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.css"]
})
export class HeaderComponent implements OnInit {
  collapsed = true;
  @Output() tabSelected = new EventEmitter<string>();
  constructor() {}

  ngOnInit() {}

  navMenuSelect(value: string) {
    this.tabSelected.emit(value);
  }
}
