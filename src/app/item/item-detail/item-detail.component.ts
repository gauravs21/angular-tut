import { Component, OnInit } from '@angular/core';
import { DataSharing } from 'src/app/commons/data-sharing.service';
import { Item } from '../item-list/item.model';

@Component({
  selector: 'app-item-detail',
  templateUrl: './item-detail.component.html',
  styleUrls: ['./item-detail.component.css'],
})
export class ItemDetailComponent implements OnInit {

  item: Item;
  desc: String;
  constructor(private dataService: DataSharing) {
    

  }

  ngOnInit() {
    this.dataService.itemData.subscribe((item:Item)=>{
      this.item =  item;
      this.desc = item.desc;
      console.log(this.item.desc)

    })
  }

  onEdit(){

    if(typeof(this.item)!=='undefined'){
      console.log(this.item.price)

    }

  }

}
