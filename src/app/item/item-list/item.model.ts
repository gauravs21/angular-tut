export class Item{
    public name: String;
    public price: number;
    public desc: String;

    constructor(name:String, desc:String, price:number){
        this.desc= desc;
        this.name =name;
        this.price = price;
    }
    
}