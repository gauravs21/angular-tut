import { Component, OnInit, Input } from '@angular/core';
import { Item } from './item.model';
import { DataSharing } from 'src/app/commons/data-sharing.service';

@Component({
  selector: 'app-item-list',
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.css']
})
export class ItemListComponent implements OnInit {
  items: Item[] = [
    new Item("Pencil", "Natraj", 50),
    new Item("Pen", "Gripper", 10),
    new Item("Notebook", "Classmate", 30)
  ];

  @Input('shop') itemData: {name:string, desc:string, price:number}

  constructor(private dataSharing: DataSharing) { }

  ngOnInit() {
    // console.log(this.items)
  }

  itemClicked(item: Item){
    this.dataSharing.itemData.next(item);
  }
}
