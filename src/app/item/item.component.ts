import { Component, OnInit } from "@angular/core";
import { Item } from "./item-list/item.model";
import { DataSharing } from '../commons/data-sharing.service';

@Component({
  selector: "app-item",
  templateUrl: "./item.component.html",
  styleUrls: ["./item.component.css"]
})
export class ItemComponent implements OnInit {

  eventEmitted = false
  constructor(private dataService: DataSharing) {}

  ngOnInit() {
    this.dataService.itemData.subscribe((item)=>{
     this.eventEmitted = true;
    //  this.dataService.itemDataVisible.emit(item);

    })
  }
}
