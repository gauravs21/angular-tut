import { Component, OnInit, EventEmitter, Output } from "@angular/core";

@Component({
  selector: "app-game-controller",
  templateUrl: "./game-controller.component.html",
  styleUrls: ["./game-controller.component.css"]
})
export class GameControllerComponent implements OnInit {
  @Output() customEventEmitter = new EventEmitter<number>();
  count = 0;
  cutsomInterval;
  intervalRunning = false;
  constructor() {}

  ngOnInit() {}

  onStartGame() {
    if (!this.intervalRunning) {
      this.intervalRunning=  true;
      this.cutsomInterval = setInterval(() => {
        this.count++;
        this.customEventEmitter.emit(this.count);
      }, 1000);
    }
  }

  onStopGame() {
    clearInterval(this.cutsomInterval);
    this.intervalRunning = false;
  }
}
