import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'demo-angular';
  itemArray = [{name:'football', desc:'nivia', price:750}];

  oddNumbers: number[] = [];
  evenNumbers: number[] = [];

  tabName= 'items';
  onCustomInterval(num:number){
    if(num %2 ===0){
      this.evenNumbers.push(num)
    } else{
      this.oddNumbers.push(num)
    }
  }

  onNavigate(value:string){
    this.tabName = value;
  }
}
