import { Directive, HostListener, HostBinding, ElementRef } from '@angular/core';

@Directive({
  selector: '[appAppDropdownDirective]'
})
export class AppDropdownDirectiveDirective {

  @HostBinding('class.open')showDrop = false;

  constructor(private elRef: ElementRef) {}
  
  @HostListener('click') toggleVisibility(){
     this.showDrop = !this.showDrop;
  }
}
