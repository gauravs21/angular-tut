import { Item } from "../item/item-list/item.model";
import { EventEmitter } from "@angular/core";
import { Subject, BehaviorSubject } from 'rxjs';

export class DataSharing {
  itemData = new EventEmitter<Item> ();
  itemDataVisible = new Subject<Item>();

  editData = new Subject<Item>();
}
