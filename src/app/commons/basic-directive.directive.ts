import { Directive, OnInit, ElementRef, Renderer2, HostListener, HostBinding } from "@angular/core";

@Directive({
  selector: "[appBasicDirective]"
})
export class BasicDirectiveDirective implements OnInit {
  ngOnInit() {
    // this.elementRef.nativeElement.style.marginTop = "8px";
    // this.elementRef.nativeElement.style.padding = "8px";

    // this.elementRef.nativeElement.style.backgroundColor = "red";
    // this.elementRef.nativeElement.style.color = "white";


    this.renderer.setStyle(this.elRef.nativeElement,'background-color', 'cyan')
    this.renderer.setStyle(this.elRef.nativeElement,'padding', '8px')
    this.renderer.setStyle(this.elRef.nativeElement,'color', 'white')
    this.renderer.setStyle(this.elRef.nativeElement,'marginTop', '8px')

  }

  constructor(private elRef: ElementRef,private renderer: Renderer2) {}


  @HostListener('mouseenter') onmouseover(event: MouseEvent){
    // this.renderer.setStyle(this.elRef.nativeElement,'background-color', 'yellow')
    this.backgroundColor = 'yellow'
  }

  @HostListener('mouseleave') onmouseleave(event: MouseEvent){
    // this.renderer.setStyle(this.elRef.nativeElement,'background-color', 'cyan')
    this.backgroundColor = 'cyan'


  }

  @HostBinding('style.backgroundColor') backgroundColor:string = 'cyan'


}


