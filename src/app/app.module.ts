import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ItemComponent } from './item/item.component';
import { PartyComponent } from './party/party.component';
import { HeaderComponent } from './header/header.component';
import { ItemListComponent } from './item/item-list/item-list.component';
import { ItemDetailComponent } from './item/item-detail/item-detail.component';
import { ItemEditComponent } from './item/item-edit/item-edit.component';
import { GameControllerComponent } from './game-controller/game-controller.component';
import { OddComponent } from './odd/odd.component';
import { EvenComponent } from './even/even.component';
import { BasicDirectiveDirective } from './commons/basic-directive.directive';
import { AppDropdownDirectiveDirective } from './commons/app-dropdown-directive.directive';
import { DataSharing } from './commons/data-sharing.service';

@NgModule({
  declarations: [
    AppComponent,
    ItemComponent,
    PartyComponent,
    HeaderComponent,
    ItemListComponent,
    ItemDetailComponent,
    ItemEditComponent,
    GameControllerComponent,
    OddComponent,
    EvenComponent,
    BasicDirectiveDirective,
    AppDropdownDirectiveDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers:[DataSharing],
  bootstrap: [AppComponent]
})
export class AppModule { }
